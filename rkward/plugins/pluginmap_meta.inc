<document>
	<snippets>
		<snippet id="author_rkward_team">
			<author
				name="RKWard Team"
				email="rkward-devel@kde.org"
				url="http://rkward.kde.org" />
		</snippet>
		<snippet id="rkward_version_guard">
			<!-- Guard against accidental inclusion of pluginmaps in a different installed version of RKWard -->
			<dependencies rkward_min_version="0.7.1z" rkward_max_version="0.7.2y" />
		</snippet>
	</snippets>
</document>
 
